using System;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

using Main;
using Utils;

namespace Drawable
{
	public class SpriteSheet : BaseSprite
	{
		Vec2<int> sheetSize;

		public SpriteSheet (string filename, int height, int width)
			: this(filename, new Vec2<int>(height, width))
		{}
		public SpriteSheet (string filename, Vec2<int> size)
			:base(Game1.g_game.CM, filename)
		{
			sheetSize = size;
		}

		public void Draw(SpriteBatch sb, int tile, Vec2<int> pos, Rectangle cam)
		{
			ViewBox = new Rectangle(tile % sheetSize.x, (tile / sheetSize.x) * sheetSize.y, sheetSize.x, sheetSize.y);
			Draw(Game1.g_game.gametime, sb);
		}
	}
}

