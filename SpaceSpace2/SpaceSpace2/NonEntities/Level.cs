using System;

using ScreenAssets;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Drawable;

namespace SpaceSpace2
{
	public abstract class Level : Screen
	{
		public struct Camera
		{
			public int height, width, x, y;
		}

		protected Room[] rooms;
		protected Room currentRoom;
		protected Camera camera = new Camera();
		protected SpriteSheet tiles;

		public Level ()
		{}

		protected void initailizeRooms (string startRoom)
		{
			//create a map for dealing with door relations when done loading
			Dictionary<string, List<Room.DoorLink>> doormap = new Dictionary<string, List<Room.DoorLink>> ();

			//create a temp list to hold rooms as we load them
			List<Room> loadedRooms = new List<Room> ();
			//find and load every room connected using a bredth-first search
			Queue<string> roomnames = new Queue<string> ();
			roomnames.Enqueue (startRoom);

			//load the given room and all rooms connected to in once and store them in rooms[]
			while (roomnames.Count > 0) {
				//load the next room to be loaded
				Room cRoom = new Room (tiles);
				List<Room.DoorLink> nextRooms = cRoom.loadRoom (roomnames.Dequeue ());
				//add the door data to the map
				doormap.Add (cRoom.roomName, nextRooms);

				//add only the rooms to the queue that we have not loaded
				foreach (Room.DoorLink dr in nextRooms) {
					bool alreadyLoaded = false;

					foreach (Room r in loadedRooms) {
						if (dr.roomLink == r.roomName) {
							alreadyLoaded = true;
							break;
						}
					}
					if (!alreadyLoaded) {
						//enqueue the room to be loaded
						roomnames.Enqueue (dr.roomLink);
					}
				}
			}

			//move all the found rooms into the rooms array
			rooms = loadedRooms.ToArray ();

			//create a door relation, perhaps this can be made more efficient later
			foreach (var e in doormap) {
				//find the room this name refers to. Throw an error if not found
				Room cRoom = findRoom(e.Key, loadedRooms);
				List<Door> doors = new List<Door>();

				foreach(var v in e.Value)
				{
					//make a new door and copy over the data
					Door d = new Door();
					d.doornum = v.doornum;
					//find the linked room
					d.linkedRoom = findRoom(v.roomLink, loadedRooms);
					doors.Add(d);
				}

				//set the doors of the room to reflect this
				cRoom.doors = doors.ToArray();
			}

			//define the current room
			currentRoom = loadedRooms[0];
		}

		private Room findRoom (string roomname, IEnumerable<Room> rooms)
		{
			Room retRoom = null;

			foreach(Room r in rooms)
			{
				if(r.roomName == roomname)
					retRoom = r;
			}
			
			if(retRoom == null)
				throw new Exception("Could not find room to link with: " + roomname);

			return retRoom;
		}
		#region implemented abstract members of Screen

		public override Screen update ()
		{
			return null;
		}

		public override void draw (SpriteBatch spriteBatch, GameTime gametime)
		{
			currentRoom.draw(spriteBatch);
		}

		#endregion
	}
}

