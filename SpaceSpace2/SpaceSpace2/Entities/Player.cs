using System;
using Drawable;
using Microsoft.Xna.Framework;
using EventManagerr;
using Main;
using Microsoft.Xna.Framework.Input;
using EventManagerr.Events;

namespace SpaceSpace2
{
	public class Player: Listener
	{

		//GameObject playerSprite;
		Vector2 _position;

		Boolean WKEY = false;
		Boolean AKEY = false;
		Boolean SKEY = false;
		Boolean DKEY = false;
		Boolean SPACE = false;

		public Vector2 position
		{
			get{return _position;}
			set{_position = value;}
		}

		public Player (Vector2 sentPos)
		{
			Game1.g_game.im.addActiveButton (Keys.W);
			Game1.g_game.im.addActiveButton (Keys.A);
			Game1.g_game.im.addActiveButton (Keys.S);
			Game1.g_game.im.addActiveButton (Keys.D);
			Game1.g_game.im.addActiveButton (Keys.Space);

			EventManager.g_EM.AddListener (new KeyboardButtonPressed(), this);
			EventManager.g_EM.AddListener (new KeyboardButtonReleased(), this);

			position = sentPos;
		}

		public void OnEvent (Event e)
		{
			//throw new NotImplementedException ();

			if (e is KeyboardButtonPressed) {

			} else if (e is KeyboardButtonReleased) {

			} else {
			}
		}
	}
}

